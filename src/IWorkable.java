/** Author: Logan Hodgins 
SOLID principles used:

Open Close Principle:
    Not applicable to the IFeedable and IWorkable interfaces.

Single Responsibility Principle:
    Not applicable to the IFeedable and IWorkable interfaces.

Interface Segregation Principle:
    The new IFeedable and IWorkable interfaces are more thin and specific 
    to their purposes, than the original IWorker interface. 
    
*/
package threesolid;

public interface IWorkable {
    public void work();
}