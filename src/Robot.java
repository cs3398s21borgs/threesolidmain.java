/** Author: Logan Hodgins
SOLID principles used:

Open Close Principle:
    By putting the Robot class into a seperate file, all of the changes when
    implementing the Robot class will go into this file. The prexisting code
    base will not need to be modified. instead the code base will be extended with
    a new Robot.java file.

Single Responsibility Principle:
    The work() method in the Robot class has only one user; the manager class.

Interface Segregation Principle:
    Not applicable to the Robot class.
*/
package threesolid;

public class Robot implements IWorkable {
    public void work() {
        // ...working
    }
}