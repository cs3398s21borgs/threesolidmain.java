/**
 * Author: Daniel Carpenter
 * SOLID Principles used:
 * 1. Open Close Principle:
 * 
 *  By putting the Worker class into a seperate file, all of the changes when
    implementing the Worker class will go into this file. The prexisting code
    base will not need to be modified. instead the code base will be extended with
    a new Worker.java file.
 * 
 * 2. Single Responsibility Principle:
 * 
 *  The work() method in the Worker class has only one user; the manager class, furthermore changes to what worker does will change the work method only.
 *  The eat() method in the Worker class is called only during lunch.  Any changes to a worker's food consumption will only affect the eat method.
 *  The fact that both methods only change in a single instance is single responsibility principle.
 * 
 * 3. Interface Segregation Principle:
 *  This file uses 2 interfaces that use the interface segregation principle: IWorkable, and IFeedable, otherwise it does not utilize the principle directly.
 * 
 */
package threesolid;

public class Worker implements IWorkable, IFeedable {
    /**
     * work method implements the IWorkable interface.
     */
    public void work() {
        //...working
    }
    /**
     * eat() method implements IFeedable interface
     */
    public void eat() {
        //...code for eating
    }
}
