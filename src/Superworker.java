/**
 * Author: Daniel Carpenter
 * SOLID Principles used:
 * 1. Open Close Principle:
 * 
 *  By putting the SuperWorker class into a seperate file, all of the changes when
    implementing the SuperWorker class will go into this file. The prexisting code
    base will not need to be modified. instead the code base will be extended with
    a new SuperWorker.java file.
 * 
 * 2. Single Responsibility Principle:
 * 
 *  The work() method in the SuperWorker class has only one user; the manager class, furthermore changes to what SuperWorker does will change the work method only.
 *  The eat() method in the SuperWorker class is called only during lunch.  Any changes to a SuperWorker's food consumption will only affect the eat method.
 *  The fact that both methods only change in a single instance is single responsibility principle.
 * 
 * 3. Interface Segregation Principle:
 *  This file uses 2 interfaces that use the interface segregation principle: IWorkable, and IFeedable, otherwise it does not utilize the principle directly.
 * 
 */
package threesolid;

public class SuperWorker implements IWorkable, IFeedable {
    /**
     * work method implements the IWorkable interface.
     */
    public void work() {
        //...working much more
    }
    /**
     * eat() method implements IFeedable interface
     */
    public void eat() {
        //...code for eating
    }
}
