/** Author: Daniel Carpenter
SOLID principles used:

Open Close Principle:
    By putting the Manager class into a seperate file, all of the changes when
    implementing the Manager class will go into this file. The prexisting code
    base will not need to be modified. instead the code base will be extended with
    a new Manager.java file.

Single Responsibility Principle:
    Manage is only responsible for managing worksers, setWorker is only responsible for changing out workers.

Interface Segregation Principle:
    Since manager is the only one managing, we decided to not separate Manage() as an interface
    so did not apply ISP, but manage does call an interface method that is ISP separated.
*/
package threesolid;

public class Manager {
	IWorkable worker;

	public void Manager() {
        break;
	}
	public void setWorker(IWorkable w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}